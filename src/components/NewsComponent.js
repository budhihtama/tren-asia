import React from 'react';
import {View, Image, Text, StyleSheet} from 'react-native';
import {wp, hp} from '../components/SettingScreen';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { convertDateToString, convertTimeToString } from '../common/date';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class NewsItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      news: {
        image: this.props.image,
        title: this.props.title,
        author: this.props.author,
        date: this.props.date,
        description: this.props.description
      },
      bookmark: false
    }
  }

  async getData(value) {
    try {
      const objValue = await AsyncStorage.getItem('bookmark')
      return objValue != null ? this.storeData(value, JSON.parse(objValue)) : this.storeData(value, null);
    } catch (e) {
      console.log('get err', e);
    }
  }

  async storeData(value, store) {
    try {
      let objValue = {}

      if(store == null ) {
        const newsStore = []
        newsStore.push(value)
        objValue = JSON.stringify(newsStore)  
      } else {
        store.push(value)
        objValue = JSON.stringify(store)

      }

      await AsyncStorage.setItem('bookmark', objValue)
      return console.log('saved');
    } catch (e) {
      console.log('store err', e);
    }
  }

  render() {
    return (
        <View style={styles.itemContainer}>
          <View>
            <Image source={{uri: this.props.image}} style={styles.itemImage} />
          </View>

          <View style={styles.itemContent}>
            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Details', this.state.news)}>
              <Text style={styles.title} numberOfLines={3}>
                {this.props.title}
              </Text>
            </TouchableWithoutFeedback>

            <Text numberOfLines={1}>
              Oleh {this.props.author}
            </Text>

            <Text>
              {convertDateToString(this.props.date)} | {convertTimeToString(this.props.date)}
            </Text>

            <View style={styles.shre}>
              <Text>Bagikan :</Text>

              <View style={styles.icns}>
                <TouchableWithoutFeedback onPress={() => alert('icns')}>
                  <Ionicons name='logo-whatsapp' size={wp(5)} color='green' style={{marginHorizontal: 4}} />
                </TouchableWithoutFeedback>

                <TouchableWithoutFeedback onPress={() => alert('icns')}>
                  <Ionicons name='logo-facebook' size={wp(5)} color='blue' style={{marginRight: 4}}/>
                </TouchableWithoutFeedback>

                <TouchableWithoutFeedback onPress={() => alert('icns')}>
                  <Ionicons name='logo-twitter' size={wp(5)} color='#1D94A8' style={{marginRight: 4}}/>
                </TouchableWithoutFeedback>

                <TouchableWithoutFeedback onPress={() => this.getData(this.state.news)}>
                  <Ionicons name='bookmark-outline' size={wp(5)} color='black' style={{marginRight: 4}}/>
                </TouchableWithoutFeedback>

              </View>

            </View>
          </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  itemContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingHorizontal: wp(5),
    marginBottom: 15,
    height: wp(35),
  },
  itemImage: {
    width: wp(30),
    height: hp(10),
    borderRadius: 10,
  },
  itemContent: {
    paddingHorizontal: 10,
    flex: 3,
  },
  title: {
    fontWeight: 'bold',
  },
  shre: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  icns: {
    flexDirection: 'row',
  }
});
