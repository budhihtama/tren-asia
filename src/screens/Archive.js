import React, { useEffect, useState, useCallback } from 'react';
import { View, Text, ScrollView, StyleSheet, Image, RefreshControl, SafeAreaView } from 'react-native';
import {wp, hp} from '../components/SettingScreen';
import Header from '../components/Header';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

const wait = (timeout) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
}

const Archive = () => {
  const [news, setNews] = useState()
  const [refreshing, setRefreshing] = useState(false);
  const [mode, setMode] = useState('light')

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => {
      getData()
      setRefreshing(false)});
  }, []);

  const getData = async() => {
    try {
      const objValue = await AsyncStorage.getItem('bookmark')
      return objValue != null ? setNews(JSON.parse(objValue)) : null;


    } catch (e) {
      console.log('get err', e);
    }
  }

  const deleteData = async(word) => {
    try {
      const newData = []
      news.filter(({title}) => {
        const testData = title.includes(word)
    
        if(!testData) {
          newData.push(author)
          return setNews(newData);
        }
      })
    } catch (e) {

    }
  }

  const getMode = async() => {
    try {
          const value = await AsyncStorage.getItem('mode');
          if (value !== null) {
            return setMode(value)
          }
        } catch (e) {
          console.log('get err', e);
        }
  }

  useEffect(() => {
    getData()
    return () => {
      getMode()
    }
  }, [news])

  return (
    <SafeAreaView style={[styles.container, {backgroundColor: mode == 'light' ? '#fcfcfc' : 'black'}]}>
        <Header />

          

          <View style={styles.content}>
            { news == null
              ? <ScrollView
                  contentContainerStyle={styles.scrollView}
                  refreshControl={
                    <RefreshControl
                      refreshing={refreshing}
                      onRefresh={onRefresh}
                    />
                  }
                >
                  <Text style={{textAlign: 'center', fontSize: wp(5), fontWeight: 'bold'}}>Pull Down to Refresh</Text>

              </ScrollView>
              : <>
                <ScrollView style={{marginBottom: wp(10)}}>

              {
                news.map((item, index) => (
                  <View key={index} style={styles.newsCard}>
                    <Image source={{uri: item.image}} style={{width: wp(30), aspectRatio: 1.3,}} />
                    <View style={styles.cardTxt}>
                      <Text style={styles.txt} numberOfLines={3}>{item.title}</Text>

                      <TouchableWithoutFeedback onPress={() => deleteData(news.title)}>
                        <Ionicons name='close-outline' size={wp(7)} color='black'/>
                      </TouchableWithoutFeedback>
                    </View>
                  </View>
                ))
              }
              </ScrollView>
              </>
            }
        </View>

        
    </SafeAreaView>
  );
};

export default Archive;

const styles = StyleSheet.create({
  content: {
    paddingHorizontal: 15,
    paddingVertical: 20
  },
  newsCard: {
    width: wp(90),
    alignSelf: 'center',
    flexDirection: 'row',
    paddingRight: 1,
    borderRadius: 4,
    overflow: 'hidden',
    backgroundColor: '#fcfcfc',
    elevation: 5,
    marginBottom: 20
  },
  cardTxt: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
  },
  txt: {
    paddingHorizontal: 10,
  },
  scrollView: {
    flex: 1,
    height: hp(80)
  },
  container: {
    flex: 1,
  }
})