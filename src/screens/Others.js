import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Header from '../components/Header'
import { wp } from '../components/SettingScreen'
import AsyncStorage from '@react-native-async-storage/async-storage';

const Others = () => {
    const [mode, setMode] = useState('light')

    const getMode = async() => {
        try {
              const value = await AsyncStorage.getItem('mode');
              if (value !== null) {
                return setMode(value)
              }
            } catch (e) {
              console.log('get err', e);
            }
      }
    
      useEffect(() => {
        getMode()
        // console.log('ok 1');
        return () => {
            getMode()
            // console.log('ok 2');
        }
      }, [mode])

    return (
        <View style={[styles.container, {backgroundColor: mode == 'light' ? '#fcfcfc' : 'black'}]}>
            <Header />

            <View style={styles.content}>
                <Text style={{color: mode == 'light' ? 'black' : '#fcfcfc'}}>Teras</Text>
                <Text style={{color: mode == 'light' ? 'black' : '#fcfcfc'}}>Trenkini</Text>
                <Text style={{color: mode == 'light' ? 'black' : '#fcfcfc'}}>Trenpasar</Text>
                <Text style={{color: mode == 'light' ? 'black' : '#fcfcfc'}}>Trendi</Text>
                <Text style={{color: mode == 'light' ? 'black' : '#fcfcfc'}}>Tech & Science</Text>

            </View>

        </View>
    )
}

export default Others

const styles = StyleSheet.create({
    container: {
        flex: 1   
    },
    content: {
        paddingVertical: 10,
        paddingHorizontal: 15,
    }
})
