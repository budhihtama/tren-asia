import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';

import Home from '../screens/Home';
import Archive from '../screens/Archive';
import Others from '../screens/Others';

const Tab = createBottomTabNavigator();

const MainNavigator = () => {
  const [mode, setMode] = useState('light')

  const getMode = async() => {
    try {
          const value = await AsyncStorage.getItem('mode');
          if (value !== null) {
            return setMode(value)
          }
        } catch (e) {
          console.log('get err', e);
        }
  }

  useEffect(() => {
      getMode()
  }, [mode])

    return (
        <Tab.Navigator
        screenOptions={({ route }) => ({
            // tabBarLabel: ({ focused, color, size}) => {
            //   let textColor;

            //   if(route.name == 'Donate') {
            //     textColor = focused ? '#1D94A8' : 'gray'
            //   } else if(route.name == 'Campaign') {
            //     textColor = focused ? '#1D94A8' : 'gray'
            //   }

            //   return (
            //     <Text style={{fontSize: 14, color: textColor, fontWeight: 'bold'}}>{route.name}</Text>
            //   )
            // },
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;
  
              if (route.name === 'Home') {
                iconName = focused
                  ? 'home-outline'
                  : 'home-outline';
              } else if (route.name === 'Archive') {
                iconName = focused ? 'bookmark-outline' : 'bookmark-outline';
              } else if (route.name === 'Others') {
                iconName = focused ? 'ellipsis-horizontal-outline' : 'ellipsis-horizontal-outline';
              }

              return <Ionicons name={iconName} size={30} color={color} />;
            },
          })}
          tabBarOptions={{
            activeTintColor: '#1D94A8',
            inactiveTintColor: 'gray',
            style: {height: 60},
            tabStyle: {paddingVertical: 7, backgroundColor: mode == 'light' ? '#fcfcfc' : 'black'}
          }}
        >
            <Tab.Screen name="Home" component={Home} />
            <Tab.Screen name="Archive" component={Archive} />
            <Tab.Screen name="Others" component={Others} />
        </Tab.Navigator>
    )
}

export default MainNavigator

const styles = StyleSheet.create({})
