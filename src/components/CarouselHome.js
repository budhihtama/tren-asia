import React from 'react';
import {Text, View, SafeAreaView, Image} from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { wp, hp} from './SettingScreen';

export default class CarouselHome extends React.Component {
    constructor(props){
      super(props);
      this.state = {
        activeIndex:0,
        carouselItems: [
        {
            title:"Item 1",
            text: "Pemerintah Pangkas PPN Rumah, Begini Rinciannya. ",
        },
        {
            title:"Item 2",
            text: "Text 2",
        },
        {
            title:"Item 3",
            text: "Text 3",
        },
      ]
    }
  }

  _renderItem({item,index}){
      return (
        <TouchableWithoutFeedback onPress={() => console.log(item)}>

          <View style={{
            backgroundColor:'floralwhite',
            borderRadius: 5,
              overflow: 'hidden',
              height: wp(45),
              width: wp(70),
              marginLeft: wp(5),
              marginRight: wp(30),
              elevation: 5
            }}>
            
            <Image 
              source={{uri: 'https://placeimg.com/640/480/any'}}
              style={{width: wp(70), height: wp(45)}} />

            <Text
              style={{
                marginBottom: wp(5),
                marginTop: 'auto',
                marginLeft: wp(5),
                marginRight: wp(5),
                color: '#fcfcfc',
              }}
              numberOfLines={2}
              >
              {item.text}
            </Text>
          </View>
        </TouchableWithoutFeedback>

      )
  }

  render() {
    
      return (
        <SafeAreaView style={{flex: 1, paddingVertical: wp(5), }}>
          <View style={{flex: 1, flexDirection:'row', justifyContent: 'center', }}>
              <Carousel
                layout={"default"}
                ref={ref => this.carousel = ref}
                data={this.state.carouselItems}
                sliderWidth={300}
                itemWidth={300}
                renderItem={this._renderItem}
                onSnapToItem = { index => this.setState({activeIndex:index})} />
          </View>

          <View>
                <Pagination
                  activeDotIndex={this.state.activeIndex}
                  dotsLength={this.state.carouselItems.length}
                  containerStyle={{paddingVertical: wp(1), marginTop: wp(2)}}
                  dotStyle={{
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    marginHorizontal: wp(0.1),
                    backgroundColor: '#0070FF'
                  }}
                  inactiveDotStyle={{
                    backgroundColor: '#C4C4C4'
                }}
                />
          </View>

        </SafeAreaView>
      );
  }
}
