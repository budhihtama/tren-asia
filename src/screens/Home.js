import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TextInput,
  Button,
} from 'react-native';
import axios from 'axios';
import NewsItem from '../components/NewsComponent';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CarouselHome from '../components/CarouselHome';
import Header from '../components/HeaderSearch';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listNews: [],
      mode: 'light'
    };
  }

  async storeData(value) {
    try {
      // String
      await AsyncStorage.setItem('mode', value);

      // Object
      // const objValue = JSON.stringify(value)
      // await AsyncStorage.setItem('@storage_Key', objValue)
    } catch (e) {
      console.log('store err', e);
    }
  }

  async getData() {
    try {
      // String
      const value = await AsyncStorage.getItem('mode');
      if (value !== null) {
        // value previously stored
        this.setState({mode: value})
      }

      // Object
      // const objValue = await AsyncStorage.getItem('@storage_Key')
      // return objValue != null ? JSON.parse(objValue) : null;
    } catch (e) {
      console.log('get err', e);
    }
  }

  componentDidMount() {
    const apiKey = '45aac2edd79eeecb0f124a353505cef2';
    axios({
      method: 'GET',
      url: `http://api.mediastack.com/v1/news?access_key=${apiKey}&countries=us&limit=10`,
    })
      .then(res => {
        // console.log('article', res.data);
        this.setState({listNews: res.data.data});
      })
      .catch(err => {
        console.log(err);
    });
    this.getData()
  }

  componentDidUpdate() {
    this.getData()
  }

  render() {
    return (
      <View style={{backgroundColor: this.state.mode == 'light' || !this.state.mode ? '#fcfcfc' : 'black'}}>
        <Header navigation={this.props.navigation} news={this.state.listNews} />

        <ScrollView>
          <CarouselHome />

          {this.state.listNews.map((news, index) => (
            <NewsItem
              key={index}
              image={news.image}
              title={news.title}
              author={news.author}
              date={news.published_at}
              description={news.description}
              navigation={this.props.navigation}
            />
          ))}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
