import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import Home from '../screens/Home';
import Archive from '../screens/Archive'
import MainNavigator from './MainNavigator';
import NewsDetails from '../screens/NewsDetails';
import Search from '../screens/Search';

const Stack = createStackNavigator();

const AppStack = () => {
  return (
    <Stack.Navigator headerMode='none'>
      <Stack.Screen name="Main" component={MainNavigator} />
      <Stack.Screen name="Details" component={NewsDetails} />
      <Stack.Screen name="Search" component={Search} />
    </Stack.Navigator>
  );
};

export default AppStack;

const styles = StyleSheet.create({});
