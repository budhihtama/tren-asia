import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableWithoutFeedback, Image } from 'react-native';
import { wp } from './SettingScreen';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Header = (props) => {
    const [mode, setMode] = useState('light')

    const changeMode = async(value) => {
        try {
            await AsyncStorage.setItem('mode', value);
            setMode(value)
          } catch (e) {
            console.log('store err', e);
          }
    }

    const getMode = async() => {
        try {
            const value = await AsyncStorage.getItem('mode');
            if (value !== null) {
              setMode(value)
            }
          } catch (e) {
            console.log('get err', e);
          }
    }

    useEffect(() => {
        getMode()
    }, [mode])

    return (
        <View style={[styles.container, {backgroundColor: mode == 'light' || !mode ? '#fcfcfc' : 'black'}]}>
            <View style={styles.content}>
                <View style={styles.blnk} />

                <View style={styles.img}>
                    <Image source={require('../assets/logo.png')} style={styles.lgo} />
                </View>

                <View style={styles.icn}>
                    <TouchableWithoutFeedback onPress={() => alert('dark mode')}>
                        <Ionicons name='moon-outline' size={wp(6)} color={mode == 'light' ? 'black' : '#fcfcfc'} />
                    </TouchableWithoutFeedback>

                </View>

            </View>
        </View>
    )
}

export default Header;

const styles = StyleSheet.create({
    container: {
        borderBottomWidth: 3,
        borderColor: 'gray',
    },
    content: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: wp(2),
    },
    img: {
        flex: 1,
    },
    lgo: {
        height: wp(10),
        width: wp(50),
        alignSelf: 'center'
    },
    icn: {
        width: wp(20),
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    blnk: {
        width: wp(20)
    }
})
