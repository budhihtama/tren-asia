import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableWithoutFeedback, Button } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { wp } from '../components/SettingScreen';
import NewsComponent from '../components/NewsComponent';
import { ScrollView } from 'react-native-gesture-handler';

const Search = (props) => {
    const [search, setSearch] = useState();
    const [searched, setSearched] = useState([]);

    const category = ['Bisnis', 'Umum', 'Damai', 'Timnas Spanyol']
    const news = props.route.params;

    const searching = () => {
        if(!search) {
            alert('Silahkan isi keyword search')
        } else {
            const newSearch = news.filter(({title}) => title.includes(search))
            return setSearched(newSearch)
        }
        
    }
    
    const categorySearching = (txt) => {
            const newSearch = news.filter(({category}) => category.includes(txt))
            return setSearched(newSearch)
    }

    // console.log(typeof search);
    
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableWithoutFeedback onPress={() => props.navigation.goBack()}>
                    <Ionicons name='arrow-back-outline' size={wp(8)} color='black' />
                </TouchableWithoutFeedback>

                <View style={styles.headerContent}>
                    <Ionicons name='search-outline' size={wp(6)} color='black' />
                    <TextInput placeholder='Masukkan Keyword' placeholderTextColor='gray' style={styles.inpt} onChangeText={text => setSearch(text)} />

                    <TouchableWithoutFeedback onPress={() => searching()}>
                        <View style={{marginLeft: 'auto', borderRadius: 4, padding: 5, backgroundColor: '#1D94A8'}}>
                                <Text style={{color: '#fcfcfc'}}>Search</Text>
                        </View>
                    </TouchableWithoutFeedback>

                </View>
            </View>

            <View style={styles.catContainer}>
                {category.map((item, index) => (
                    <TouchableWithoutFeedback key={index} onPress={() => categorySearching('general')}>
                        <View style={styles.cats}>
                            <Text>{item}</Text>
                        </View>
                    </TouchableWithoutFeedback>
                ))}
            </View>

            {
                searched == null || searched.length == 0
                ? null
                : <ScrollView style={styles.content}>
                    {searched.map((news, index) => (
                        <NewsComponent
                            key={index}
                            image={news.image}
                            title={news.title}
                            author={news.author}
                            date={news.published_at}
                            description={news.description}
                            navigation={props.navigation}
                        />
                    ))}
                </ScrollView>
            }
        </View>
    )
}

export default Search

const styles = StyleSheet.create({
    container: {   
        paddingHorizontal: 15,
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 15,
        height: wp(15),
    },
    headerContent: {
        flexDirection: 'row',
        alignItems: 'center',
        width: wp(80),
        borderBottomWidth: 1,
        borderColor: 'gray',
    },
    inpt: {
        paddingBottom: 0,
        marginLeft: 5,
    },
    catContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignSelf: 'center',
        width: wp(80)
    },
    cats: {
        backgroundColor: '#c4c4c4',
        paddingVertical: 3,
        paddingHorizontal: 5,
        borderRadius: 5
    },
    content: {
        marginTop: 20,
        marginBottom: wp(30)
    }
})
