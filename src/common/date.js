export const convertDateToString = (selectedDate) => {
    selectedDate = new Date(selectedDate);
    const currDate = selectedDate.getDate();
    let currMonth = selectedDate.getMonth() + 1;
    const currYear = selectedDate.getFullYear();

    if(currMonth == 1) {
        currMonth = 'Januari'
    } else if(currMonth == 2) {
        currMonth = 'Februari'
    } else if(currMonth == 3) {
        currMonth = 'Maret'
    } else if(currMonth == 4) {
        currMonth = 'April'
    } else if(currMonth == 5) {
        currMonth = 'Mei'
    } else if(currMonth == 6) {
        currMonth = 'Juni'
    } else if(currMonth == 7) {
        currMonth = 'Juli'
    } else if(currMonth == 8) {
        currMonth = 'Agustus'
    } else if(currMonth == 9) {
        currMonth = 'September'
    } else if(currMonth == 10) {
        currMonth = 'Oktober'
    } else if(currMonth == 11) {
        currMonth = 'November'
    } else {
        currMonth = 'Desember'
    }

    return (
        currDate+ ' ' + currMonth + ' ' +currYear
    );
};

export const convertTimeToString = (selectedTime) => {
    selectedTime = new Date(selectedTime)
    const currHour = ('0' + selectedTime.getHours()).slice(-2);
    const currMinute = ('0' + selectedTime.getMinutes()).slice(-2);
    return currHour + ':' + currMinute;
};