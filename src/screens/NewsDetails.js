import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { convertDateToString, convertTimeToString } from '../common/date';
import Header from '../components/Header';
import { wp } from '../components/SettingScreen';

const NewsDetails = (props) => {
    const news = props.route.params;

    return (
        <View>
            <Header />

            <View style={styles.content}>
                <Text style={styles.titleTxt}>{news.title}</Text>

                <View style={{flexDirection: 'row', marginVertical: 10}}>
                    <Text style={styles.dateTxt}>{`${convertDateToString(news.date)}, `}</Text>
                    <Text style={styles.dateTxt}>{convertTimeToString(news.date)}</Text>
                </View>

                <Text>Penulis : {news.author}</Text>

                <Image source={{uri: news.image}} style={styles.img} />

                <Text>{news.description}</Text>
            </View>
        </View>
    )
}

export default NewsDetails

const styles = StyleSheet.create({
    content: {
        paddingHorizontal: 16,
        paddingVertical: 20
    },
    titleTxt: {
        borderBottomWidth: 2,
        borderColor: 'gray',
        paddingBottom: 16,
        fontSize: wp(5),
        fontWeight: 'bold'
    },
    img: {
        width: wp(70),
        aspectRatio: 1.5,
        alignSelf: 'center',
        marginVertical: 20,
        resizeMode: 'contain',
        borderRadius: 5
    }
})
